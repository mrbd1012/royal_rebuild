@extends('layouts.index')
@section('meta')
    
@stop
@section('title')

@stop
@section('style')
    
@stop
@section('breadcrumbs')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Stater Page
                    <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Compass</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Sample Pages</a></li>
                    <li class="breadcrumb-item active">Stater Page</li>
                </ul>
            </div>
        </div>
    </div>
@stop
@section('content')
    <div class="card">
        <div class="body">
            <h4>Stater page</h4>
        </div>
    </div>
@stop
@section('script')
    
@stop