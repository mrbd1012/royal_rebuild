<aside id="leftsidebar" class="sidebar">
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <div class="image"><a href="profile.html"><img src="{{ asset('template/main/assets/images/profile_av.jpg') }}" alt="User"></a></div>
                    <div class="detail">
                        <h4>Michael</h4>
                        <small>UI UX Designer</small>
                    </div>
                    <a href="events.html" title="Events"><i class="zmdi zmdi-calendar"></i></a>
                    <a href="mail-inbox.html" title="Inbox"><i class="zmdi zmdi-email"></i></a>
                    <a href="contact.html" title="Contact List"><i class="zmdi zmdi-account-box-phone"></i></a>
                    <a href="chat.html" title="Chat App"><i class="zmdi zmdi-comments"></i></a>
                    <a href="sign-in.html" title="Sign out"><i class="zmdi zmdi-power"></i></a>
                </div>
            </li>
            <li class="header">MAIN</li>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a>
                <ul class="ml-menu">
                    <li><a href="index.html">Main</a> </li>
                    <li><a href="dashboard-rtl.html">RTL</a></li>
                    <li><a href="index2.html">Horizontal</a></li>
                    <li><a href="ec-dashboard.html">Ecommerce</a></li>
                    <li><a href="blog-dashboard.html">Blog</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>