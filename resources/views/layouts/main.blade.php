<!doctype html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="#">
    @yield('meta')
    <title>@yield('title', config('app.name'))</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('template/main/assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('template/main/assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('template/main/assets/css/color_skins.css') }}">
    @yield('style')
</head>
<body class="theme-cyan">
<!-- Page Loader -->
@include('layouts.partials.main.page_loader')

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Top Bar -->
@include('layouts.partials.main.top_bar')

<!-- Left Sidebar -->
@include('layouts.partials.main.left_sidebar')


<!-- Main Content -->
<section class="content">
    @yield('breadcrumbs')

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                @yield('content')
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js -->
<script src="{{ asset('template/main/assets/bundles/libscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('template/main/assets/bundles/vendorscripts.bundle.js') }}"></script> <!-- Lib Scripts Plugin Js -->

<script src="{{ asset('template/main/assets/bundles/mainscripts.bundle.js') }}"></script>
@yield('script')
</body>

</html>
